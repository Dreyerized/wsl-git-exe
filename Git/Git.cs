﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace Git
{
    class Git
    {
        static void Main(string[] args)
        {
            string processArguments;
            if (args.Length > 1 && args[0] == "difftool")
            {
                processArguments = string.Join(" ", args.Skip(2).Select(ProcessDifftoolArgument));
                RunGitDifftool(args[1], args.Last(), processArguments);
            }
            else
            {
                processArguments = string.Join(" ", args.Select(ProcessGitArguments));
                RunGitCommand(processArguments);
            }
        }

        private static void RunGitCommand(string processArguments)
        {
            Process git = new Process
            {
                StartInfo =
                {
                    FileName = "bash.exe",
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    Arguments = $"-c \"git {processArguments} && echo ''\""
                }
            };
            git.Start();

            MonitorProcess(git, true, processArguments);
        }

        private static void RunGitDifftool(string difftoolPath, string fileName, string processArguments)
        {
            if (difftoolPath == "code")
            {
                difftoolPath = File.Exists(VS_CODE_64_BIT) ? VS_CODE_64_BIT : VS_CODE_32_BIT;
            }
            Process difftool = new Process
            {
                StartInfo =
                {
                    FileName = difftoolPath,
                    CreateNoWindow = true,
                    RedirectStandardError = true,
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    Arguments = processArguments
                }
            };
            difftool.Start();
            Console.WriteLine($"Opening diff for {fileName} with {difftoolPath}");

            MonitorProcess(difftool);
        }

        private static void MonitorProcess(Process process, bool modifyOutput = false, string processArguments = "")
        {
            WriteStandardOutput(process, modifyOutput, processArguments);
            WriteStandardError(process);
            process.WaitForExit();
        }

        private static void WriteStandardOutput(Process process, bool modifyOutput, string processArguments)
        {
            string standardOutput = process.StandardOutput.ReadToEnd();
            if (standardOutput.Length > 0 && modifyOutput && processArguments.StartsWith("rev-parse"))
            {
                standardOutput = standardOutput.Replace("/mnt/c/", "c:\\").Replace('/', '\\');
            }
            Console.WriteLine(standardOutput);
        }

        private static void WriteStandardError(Process process)
        {
            Console.WriteLine(process.StandardError.ReadToEnd());
        }

        private static string ProcessDifftoolArgument(string argument)
        {
            string modifiedArgument = argument;
            string windowsMountPath = "/mnt/c/";
            if (argument.StartsWith(windowsMountPath))
            {
                modifiedArgument = argument.Replace(windowsMountPath, "c:\\").Replace('/', '\\');
            }

            return EscapeArgument(modifiedArgument);
        }

        private static string ProcessGitArguments(string arguments)
        {
            string modifiedArguments = arguments;
            string cRoot = @"c:\";
            if (arguments.Contains(cRoot))
            {
                modifiedArguments = arguments.Replace(cRoot, "/mnt/c/").Replace('\\', '/');
            }

            return EscapeArgument(modifiedArguments);
        }

        private static string EscapeArgument(string argument)
        {
            return (argument.Contains(' ') ? $"\\\"{argument}\\\"" : argument).Replace('\\', '/');
        }

        private const string VS_CODE_64_BIT = @"C:\Program Files\Microsoft VS Code\Code.exe";
        private const string VS_CODE_32_BIT = @"C:\Program Files (x86)\Microsoft VS Code\Code.exe";
    }
}
